provider "aws" {
  region = "ap-southeast-1"
  version = "~> 2.24"
  profile = "${var.awsprofile}"
}

############### GLOBAL STATE BACKEND ###############
terraform {
  backend "s3" {
    region  = "ap-southeast-1"
    encrypt = true
  }
}
####################################################
############ DEMO API ############################
#####################################################
module "deployment_group" {
  source              = "git::ssh://git@bitbucket.org/hlmmuham/chatbot-elb-deployment-tf-modules.git"

  # aws_ami.tf - Get Latest AMI
  packer_ami_name     = "${var.ami_name}"

  # aws_asg.tf - Create ASG & ELB
  instance_type       = "${var.instance_type}"
  key_name            = "${var.key_name}" 
  min_size            = "${var.min_size}"
  max_size            = "${var.max_size}"
  

  # aws_lt.tf - Create Launch template
  volume_size         = "${var.volume_size}"
  volume_type         = "${var.volume_type}"
  placement_tenancy    = "${var.placement_tenancy}"

  # aws_asg_metric_policy.tf (N/A)
  scale_up_threshold      = "${var.scale_up_threshold}" 
  scale_down_threshold    = "${var.scale_down_threshold}" 
  metric_name             = "${var.metric_name}" 
  
  # aws_code_deploy.tf - Deployment Group
  app_name            = "${data.terraform_remote_state.codedeploy.outputs.codedeploy_app_name}"
  vpc_zone_identifier = "${data.aws_subnet_ids.public_subnet_ids.ids}"
  vpc_asg_private_subnet = "${data.aws_subnet_ids.private_subnet_ids.ids}"
  vpc_id_apg          = "${data.aws_vpc.vpc_id.id}"
  certificate_arn     = "${data.aws_acm_certificate.cert_global.arn}"
  #subnets_alb         = "${data.aws_subnet_ids.public_subnet_ids.ids}"
  s3_bucket_name      = "${var.s3_bucket_name}"
  trigger_target_arn  = "${local.trigger_target_arn}"
  
  # aws_load_balancer.tf
  listener_instance_port = "${var.listener_instance_port}"
  health_check_target  = "${var.health_check_target}"

  # aws_security_group.tf
  vpc_cidr              = "${var.vpc_cidr}"
  ingress_from_port_01  = "${var.ingress_from_port_01}"
  ingress_from_port_02  = "${var.ingress_from_port_02}"
  ingress_to_port_01    = "${var.ingress_to_port_01}"
  ingress_to_port_02    = "${var.ingress_to_port_02}"


  # Tags
  Application_ID      = "${var.Application_ID}"
  Application_Role    = "${var.Application_Role}"
  Business_Owner      = "${var.Business_Owner}"
  Environment         = "${var.Environment}"
  Technical_Owner     = "${var.Technical_Owner}"
  CrossBUWorkload     = "${var.CrossBUWorkload}"  
  Service_Name        = "${var.Service_Name}"
  Project             = "${var.Project}"
  OS_Version          = "${var.OS_Version}"
  Team                = "${var.Team}"
  Name                = "${var.Name}"
  Automation_Schedule_Opt_Out   = "${var.Automation_Schedule_Opt_Out}"
  Automation_Schedule_UTC       = "${var.Automation_Schedule_UTC}"
  Business_Unit       = "${var.Business_Unit}"
  Cluster             = "${var.Cluster}"
  Compliance          = "${var.Compliance}"
  Security            = "${var.Security}"
  Confidentiality     = "${var.Confidentiality}"
  Version             = "${var.build_version}"

}
