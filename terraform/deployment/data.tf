
data "terraform_remote_state" "codedeploy" {
  backend = "s3"
  config  = {
    bucket  = "${var.state_bucket_name}"
    key     = "${var.state_dir}/app.tfstate"
    region  = "ap-southeast-1"
    profile = "${var.awsprofile}"
  }
}

###############################################################
data "aws_acm_certificate" "cert_global" {
  domain      = "${var.domain}"
  types       = ["${var.acm_type}"]
  statuses    = ["ISSUED"]
  most_recent = true
}

###############################################################
data "aws_vpc" "vpc_id" {
  tags = {
    Name = "${var.vpc_name}"
  }
}

data "aws_subnet_ids" "public_subnet_ids" {
  vpc_id = "${data.aws_vpc.vpc_id.id}"
  
  filter {
    name    = "tag:Name"
    values  = ["${var.public_subnet_name}1a", "${var.public_subnet_name}1b"]
  }
}

###############################################################
data "aws_subnet_ids" "private_subnet_ids" {
  vpc_id = "${data.aws_vpc.vpc_id.id}"
  
  filter {
    name = "tag:Name"
    values = ["${var.private_subnet_name}1a", "${var.private_subnet_name}1b"]
  }
}

#data "aws_subnet_ids" "internal_subnet_ids" {
#  vpc_id = "${data.aws_vpc.vpc_id.id}"
#  
#  filter {
#    name = "tag:Name"
#    values = ["${var.internal_subnet_name}-1a", "${var.internal_subnet_name}-1b"]
#  }
#}