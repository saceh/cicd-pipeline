provider "aws" {
    region = "ap-southeast-1"
    version = "~> 2.24"
    profile = "${var.awsprofile}"
}

module "code_application" {
  source = "https://saceh@bitbucket.org/saceh/codedeploy-application-tf-modules.git"
  codedeploy_application_name = "${var.codedeploy_application_name}"
  compute_platform = "${var.compute_platform}"

}

##################################################

output "codedeploy_app_name" {
  value = "${module.code_application.codedeploy_app_name}"
}

output "codedeploy_app_id" {
  value = "${module.code_application.codedeploy_app_id}"
}

##################################################

variable "compute_platform" {}
variable "codedeploy_application_name" {}
variable "awsprofile" {
  default = "demo"
}

#################### GLOBAL STATE BACKEND ####################
terraform {
  backend "s3" {
    region  = "ap-southeast-1"
    encrypt = true
  }
}
############################################################
