variable "awsprofile" {default = "apg-nonprod"}

##########
### NETWORK
##########
variable "vpc_name" {}
variable "public_subnet_name" {}
variable "private_subnet_name" {}
variable "domain" {}
variable "acm_type" {}
variable "ami_name" {}
variable "vpc_cidr" {}



##########
### TAGS
##########
variable "Application_ID" {}
variable "Application_Role" {}
variable "Business_Owner" {}
variable "Environment" {}
variable "Technical_Owner" {}
variable "CrossBUWorkload" {}
variable "Service_Name" {}
variable "Project" {}
variable "OS_Version" {}
variable "Team" {}
variable "Name" {}
variable "Automation_Schedule_Opt_Out" {}
variable "Automation_Schedule_UTC" {}
variable "Business_Unit" {}
variable "Cluster" {}
variable "Compliance" {}
variable "Security" {}
variable "build_version" {}
variable "Confidentiality" {}


##########
## AWS
##########
#variable "vpc_id_apg" {}
#variable "vpc_zone_identifier" {}
#variable "availability_zones" {}
#variable "subnets_alb" {}

# variable "packer_ami_name" {}
variable "s3_bucket_name" {}
variable "instance_type" {}
variable "key_name" {}
variable "min_size" {}
variable "max_size" {}

# Launch Template
variable "volume_size" {}
variable "volume_type" {}
variable "placement_tenancy" {}

# AutoScaling Policy Settings
variable "scale_up_threshold" {
    default = "50"
}
variable "scale_down_threshold" {
    default = "5"
}
variable "metric_name" {
    default = "CPUUtilization"
}


### CodeDeploy Trigger
locals {
    env = "${terraform.workspace}"

    target_arn_list = {
        "dev"       = "arn:aws:sns:ap-southeast-1:282288194755:ChatBot-Deployment-Status"
        "stage"     = "arn:aws:sns:ap-southeast-1:282288194755:ChatBot-Deployment-Status"
        "prod"      = "arn:aws:sns:ap-southeast-1:777474632788:ChatBot-Deployment-Status"
    }

    trigger_target_arn = "${lookup(local.target_arn_list, local.env)}"
}

variable "state_dir" {}
variable "state_bucket_name" {}

variable "listener_instance_port" {}

variable "ingress_to_port_01" {}
variable "ingress_to_port_02" {}
variable "ingress_from_port_02" {}
variable "ingress_from_port_01" {}
#variable "trigger_target_arn" {}
variable "health_check_target" {}

