# Project Title

DevOps: Creating AWS CodeDeploy Resources Infrastructure As Codes (IAC) via HashiCorp Packer

## Getting Started

These instructions will get you a copy of the project up and running for development and testing purposes. 
See **deployment** for notes on how to deploy the project on a live system.

``` 
Author's Notes For Developers:
1. Control your resources via terraform/deployment/env/$ENV_NAME.tfvars
2. Beware that some resources' naming depends on taggings (i.e. ASG_tag_name = ${ApplicationID}-ASG)
3. CAUTION: Please make sure to use different S3 bucket between $ENV_NAME as they are globally unique

```

### Steps to add new variables (Future Implementation)
* Module changes
    - [x] Go to variables.tf: + variable "example" {}
    - [x] Go to example.tf: + metric_example = "${var.metricexample}"
* IAC-Repository changes
    - [x] Go to variables.tf: + variable "example" {}
    - [x] Go to /terraform/env/$ENV_NAME.tf: + metric_name = "metric_value" { default = "1" (if needed) }
    - [x] Go to /terraform/deployment/$SERVICE_NAME.tf: + metric_example = "${var.metric_name}" [Note: ~var.metric_value~]

### Prerequisites (Local Testing)

1. [AWS CLI Tools](https://aws.amazon.com/cli/)
2. [HashiCorp Terraform](https://www.terraform.io/downloads.html)
3. [HashiCorp Packer](https://www.packer.io/downloads.html)
4. [Git Package ](https://git-scm.com/downloads.html)
5. [Bash Environment* ](http://tldp.org/LDP/Bash-Beginners-Guide/html/chap_03.html)

## Deployment

The steps taken by Terraform are in such order:

### Importing
1. Import CodeDeploy Application (/terraform/codedeploy-app/) S3 statefiles
2. Import ACM certificate, VPC (via name), Subnets_ID (via name)
3. Define CodeDeploy Deployment Group backend - Destination S3
4. Launch modules - ELB, ASG, CodeDeployDG & Security Groups
5. (KIV) Launch modules - RDS

### Modules Resources (aws-elb-deployment-tf-modules)
* LT
    - [x] aws_ami.tf
    - [x] aws_lt.tf
* ASG
    - [x] aws_asg.tf
    - [x] aws_asg_metric_policy.tf
* ELB (SG: EC2 & ELB)
    - [x] aws_load_balancer.tf
    - [x] aws_security.group.tf
* CodeDeploy (Deployment Groups & S3)
    - [x] aws_s3_bucket.tf
    - [x] aws_code_deploy.tf
- [x] aws_iam_role.tf
- [x] tags.tf
- [x] variables.tf

### Terraform (Prerequisite Steps)

#### 1. terraform validate   - to get script format checked
#### 2a. terraform init       -to get initialization files checked and running
#### 2b. terraform refresh   - to get output refreshed without running "terraform init" or "terraform plan"
#### 3. terraform plan -var-file="env/$ENV_NAME.tfvars" - to import variables and plan a terraform project with it

## Built With (Prod)

* [Bitbucket Pipelines](https://bitbucket.org/product/features/pipelines) - The CICD tool
* [HashiCorp Terraform](https://www.terraform.io/) - Cloud Resource Management
* [HashiCorp Packer ](https://www.packer.io/) - AMI Provisioning Tool

## Authors

* **Chris Lim, Wei Yew** - *Initial work* - [chriswylim](https://github.com/chriswylim)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the Astro-MEASAT Broadcast Network Systems License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
