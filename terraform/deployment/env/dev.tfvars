############################################################################
############################  DEVELOPMENT ENV   ############################                      
############################################################################
#################################
##  TAGS
#################################
Application_ID              =  "Demo Apps"
Application_Role            =  "Demo"
Automation_Schedule_Opt_Out = "YES"
Automation_Schedule_UTC     = "UPTIME-ALWAYSON"
Business_Owner              =  "msallehuddin.ukm@gmail.com"
Business_Unit               = "Demo"
Cluster                     = "NO"
Compliance                  = "NO"
CrossBUWorkload             = "NO"
Environment                 = "DEV"
Name                        = "DEMO-DEV" # instance name
OS_Version                  = "CENTOSO72"
Project                     = "Demo"
Team                        = "Demo"
Security                    = "NO"
Service_Name                = "Demo-API"
Technical_Owner             = "msallehuddin.ukm@gmail.com"
Confidentiality             = "NO"
build_version               = "1.0"


########################################
## APP Resource
######################################
# Project-Based Variables
domain                  = "*.domain.com.my"
acm_type                = "IMPORTED"
s3_bucket_name          = "demo-codedeploy-dev"

# Environment-Based Variables
## Launch Template (LT)
volume_size             = "30"
volume_type             = "gp2"
placement_tenancy       = "default"

## Auto-Scaling Group (ASG)
instance_type           = "t2.mmicro"
key_name                = "demo-nonprod"
min_size                = "1"
max_size                = "2"

# Security Group EC2 Ingress Rule INBOUND
ingress_from_port_01  = "5000"
ingress_from_port_02  = "3000"
ingress_to_port_01    = "5000"
ingress_to_port_02    = "3000"

## Elastic Load Balancer (ELB)
vpc_name                = "DemoVPC"
public_subnet_name      = "DemoPublicSubnet"
private_subnet_name     = "DemoprivateSubnet"
health_check_target     = "TCP:3000"
vpc_cidr                = "10.0.0.0/24"
listener_instance_port  = "3000"


## CloudWatch Scaling Policies
scale_up_threshold      = 70
scale_down_threshold    = 10
metric_name             = "CPUUtilization"

# SNS ARN
#trigger_target_arn = "arn:aws:sns:ap-southeast-1:282288194755:Demo-Deployment-Status"
